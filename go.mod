module WB_level0

go 1.21

require (
	github.com/lib/pq v1.10.9
	github.com/nats-io/nats.go v1.31.0
	github.com/nats-io/stan.go v0.10.4
)

require (
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/klauspost/compress v1.17.3 // indirect
	github.com/nats-io/nats-server/v2 v2.10.6 // indirect
	github.com/nats-io/nats-streaming-server v0.25.6 // indirect
	github.com/nats-io/nkeys v0.4.6 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	golang.org/x/time v0.4.0 // indirect
)

require (
	github.com/labstack/echo/v4 v4.11.3
	github.com/labstack/gommon v0.4.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	golang.org/x/crypto v0.15.0 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sys v0.14.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
